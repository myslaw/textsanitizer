use std::{
    collections::HashMap,
    env,
    fs::File,
    io::{BufRead, BufReader, BufWriter, Write}, vec,
};

fn main() -> Result<(), String> {
    let Some((input_path, output_path)) = resolve_args(env::args()) else {
        println!("Please use -i to define input file to be sanetized. Optionally use -o to define custom sanetized file path.");
        return Ok(());
    };
    println!("Intializing objects...");
    let grouper = Grouper::new();
    let repository = WordRepository::of_path(&"resources/odm.txt".to_owned())?;
    let sanitizer = Sanitizer::new(grouper, repository);
    println!("Starting sanetization...");

    if let Err(description) = sanitizer.sanetize_file(&input_path, &output_path) {
        println!("Failed to sanitize from {input_path} to {output_path}, because: {description}");
        return Err(description);
    }
    println!("Finished sanetization. Santeized file located at {output_path}");
    return Ok(());
}

fn resolve_args(args: env::Args) -> Option<(String, String)> {
    let vector: Vec<String> = vec!["-i".to_owned(), "S:\\video\\gentlemen\\The.Gentlemen.2019.1080p.BluRay.x265-RARBG\\Subs\\3\\The.Gentlemen.2019.SWESUB.1080p.BluRay.H265.AAC.Mr_KeFF.txt".to_owned()];
    println!("All args {:?}", &vector);
    let mut args_iter = vector.into_iter();
    let mut input_path: Option<String> = None;
    let mut output_path: Option<String> = None;
    while let Some(arg) = args_iter.next() {
        match arg.as_str() {
            "-i" | "--input" => {
                input_path = args_iter.next();
            }
            "-o" | "--output" => {
                output_path = args_iter.next();
            }
            _ => (),
        }
    }
    let Some(input_path) = input_path else {
        return None;
    };
    let output_path = output_path.unwrap_or_else(|| add_suffix(&input_path, "_fixed"));
    return Some((input_path, output_path));
}

fn add_suffix(path: &str, suffix: &str) -> String {
    let mut result_path = path.to_owned();
    let Some(pos) = result_path.rfind('.') else{
        return result_path + suffix;
    };
    result_path.insert_str(pos, suffix);
    return result_path;
}

///For each given line it should output a Vector of tuples (bool, &str). If the tuple has a bool set to TRUE, it should be sanitized as is and appended to result, otherwise just appended to result.
struct Grouper {}

impl Grouper {
    pub fn new() -> Grouper {
        Grouper {}
    }

    pub fn group<'a>(&self, raw: &'a String) -> Vec<(bool, &'a str)> {
        let mut buffer: Vec<(bool, &str)> = Vec::with_capacity(5);
        let mut was_alphabetic = raw.starts_with(|c: char| c.is_alphabetic());
        let mut beg: usize = 0;

        for (index, a_char) in raw.chars().enumerate() {
            if a_char.is_alphabetic() != was_alphabetic {
                buffer.push((was_alphabetic, &raw[beg..index]));
                beg = index;
                was_alphabetic = !was_alphabetic;
            }
        }
        buffer.push((was_alphabetic, &raw[beg..]));
        return buffer;
    }
}

struct Sanitizer {
    grouper: Grouper,
    word_repository: WordRepository,
    should_sanitize_line_fn: fn(&str, usize) -> bool,
}

impl Sanitizer {
    pub fn new(grouper: Grouper, word_repository: WordRepository) -> Sanitizer {
        Sanitizer {
            grouper,
            word_repository,
            should_sanitize_line_fn: |_, _| true, //FIXME
        }
    }

    pub fn sanetize_file(&self, input_path: &String, output_path: &String) -> Result<(), String> {
        let Ok(mut input_reader) = File::open(input_path).map(|f| BufReader::new(f)) else {
            return Err(format!("Failed to open input file located at {input_path}."));
        };
        let Ok(mut output_writer) = File::create(output_path).map(|f| BufWriter::new(f)) else {
            return Err(format!("Failed to open output file located at {output_path}."));
        };

        let mut line_number: usize = 0;
        let mut buf: String = String::with_capacity(128);
        while let Ok(byte_count) = input_reader.read_line(&mut buf) {
            if byte_count == 0 {
                return Ok(());
            }
            if (self.should_sanitize_line_fn)(&buf, line_number){
                let sanitized = self.sanitize_line(&buf);
                let Ok(_) = output_writer.write_all(sanitized.as_bytes()) else{
                    panic!("Error occured while writing to {output_path}"); //FIXME
                };
            }
            line_number += 1;
            buf.clear();
        }
        Ok(())
    }

    pub fn sanitize_line(&self, line: &String) -> String {
        let groups = self.grouper.group(line);
        let mut sanitized = String::with_capacity(line.capacity());
        for (active, raw) in groups {
            if active {
                sanitized.push_str(self.word_repository.find_word_like(raw).unwrap_or(raw));
            }else {
                sanitized.push_str(raw);
            }
        }
        return sanitized;
    }
}

struct WordRepository {
    ///word length to list of words with such length
    length_to_words: HashMap<usize, Vec<String>>,
}

impl WordRepository {
    pub fn of_path(path: &String) -> Result<WordRepository, String> {
        let Ok(reader) = File::open(path).map(|f| BufReader::new(f)) else {
            return Err(format!("Failed to read dictionary located at {path}"));
        };
        let mut length_to_words: HashMap<usize, Vec<String>> = HashMap::new();
        for line in reader.lines() {
            let Ok(line) = line else {
                return Err(format!("Failed to read a line from dictionary: {path}."));
            };
            if !line.chars().all(|c| c.is_ascii()) {
                length_to_words
                    .entry(line.chars().count())
                    .or_insert_with(Vec::new)
                    .push(line);
            }
        }
        length_to_words.values_mut().for_each(|words| words.sort());
        return Ok(WordRepository { length_to_words });
    }

    pub fn find_word_like(&self, corrupted_word: &str) -> Option<&str> {
        let Some(words) = self.length_to_words.get(&corrupted_word.chars().count()) else {
            return None;
        };
        let mut best_word= None;
        let mut best_similarity: usize = 0;
        for word in words {
            let temp_similarity = WordRepository::calc_similarity(corrupted_word, word);
            if temp_similarity > best_similarity {
                best_word = Some(&word[..]);
                best_similarity = temp_similarity;
            }
        }
        return best_word;
    }

    ///Find how similar two strings are. Both strings are assumed to be of the same length.
    fn calc_similarity(corrupted_word: &str, valid_word: &String) -> usize {
        let mut similarity = 0;
        let mut valid_word_iter = valid_word.chars();
        for corrupted_c in corrupted_word.chars() {
            let Some(valid_c) = valid_word_iter.next() else {
                panic!("Failed to iterate over valid_word: {valid_word}");
            };
            if corrupted_c == valid_c {
                similarity += 1;
            }
        }
        return similarity;
    }
}